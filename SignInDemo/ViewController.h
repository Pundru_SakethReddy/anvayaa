//
//  ViewController.h
//  SignInDemo
//
//  Created by Saketh Reddy on 28/02/17.
//  Copyright © 2017 SakethReddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface ViewController : UIViewController<GIDSignInUIDelegate>


@end

