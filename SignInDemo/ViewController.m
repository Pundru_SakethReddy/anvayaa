//
//  ViewController.m
//  SignInDemo
//
//  Created by Saketh Reddy on 28/02/17.
//  Copyright © 2017 SakethReddy. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "SuccessViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    if ([GIDSignIn sharedInstance].hasAuthInKeychain) {
        NSLog(@"Signed in");
        [[GIDSignIn sharedInstance] signInSilently];
        SuccessViewController *svc = [[SuccessViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
        
    } else {
        NSLog(@"Not signed in");
    }
    
    
    

    
//    ViewController *AppDelegate = (ViewController *)[[UIApplication sharedApplication] delegate];
    
    
}
- (IBAction)didTapSignOut:(id)sender {
    
    [[GIDSignIn sharedInstance] signOut];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
